(defproject sqlrest "0.1.0-SNAPSHOT"

  :description "Auto-generate REST API from SQL-queries"

  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/data.json "0.2.5"]
                 [mysql/mysql-connector-java "5.1.33"]
                 [compojure "1.2.0"]
                 [ring "1.3.1"]
                 [yesql "0.4.0"]
                 [environ "1.0.0"]]

  :profiles {:dev {:plugins [[lein-expectations "0.0.8"]]
                   :dependencies [[expectations "2.0.12"]]}}

  :plugins [[lein-environ "1.0.0"]
            [lein-ring "0.8.12"]]

  :ring {:handler sqlrest.core/handler})










