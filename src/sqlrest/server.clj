(ns sqlrest.server
  (:require [ring.adapter.jetty :as jetty]
            [sqlrest.core :refer [handler]]))

(defonce server (atom nil))

(defn start-server [& [port]]
  (let [port (if port (Integer/parseInt port) 3000)]
    (reset! server (jetty/run-jetty #'handler {:port port :join? false}))
    (println (str "You can view the site at http://localhost:" port))))

(defn stop-server []
  (.stop @server)
  (reset! server nil))

