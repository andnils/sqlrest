(ns sqlrest.core
  (:require [clojure.data.json :as json]
            [compojure.core :refer [routes defroutes GET]]
            [environ.core :refer [env]]
            [ring.middleware.params :refer [wrap-params]]
            [yesql.core :refer [defqueries]]))

(defqueries "sql/queries.sql")

(defn find-queries []
  (filter
   (fn [[_ v]] (:yesql.types/source (meta v)))
   (ns-publics *ns*)))

(defn route-generator [[sym fun]]
  (let [args (-> (meta fun) :arglists first rest)]
    `(GET ~(str "/" (name sym))
          ~(vec args)
          (json/write-str ((partial ~fun (env :dbspec)) ~@args)))))

(def handler
  (->> (find-queries)
       (map route-generator)
       (map eval)
       (apply routes)
       (wrap-params)))
